import express from 'express';
import {NationApp} from './nation';
import path from 'path';

const app: express.Application = express();



app.set('view engine', 'ejs');
app.set('views', __dirname + '/../views');
app.use(express.static(path.join(__dirname + '/../static')));

//URLS
app.use('/api', NationApp);
export default app;