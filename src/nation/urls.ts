import {Router, Request, Response} from 'express';
import {NationViews} from './views';

const router: Router = Router()

router.get('/', (req: Request, res: Response) => {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.json(NationViews());
});

export const NationApp: Router = router;