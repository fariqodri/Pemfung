export function students(): object[] {
    return [
        {
            name: (): string => 'Andika Hanavian Atmam',
            npm: (): string => '1606918585'
        },
        {
            name: (): string => 'Fari Qodri Andana',
            npm: (): string => '1606875964'
        },
        {
            name: (): string => 'Indra Septama',
            npm: (): string => '1606892296'
        },
        {
            name: (): string => 'Yumna Pratista Tastaftian',
            npm: (): string => '1606836976'
        },
    ];
};

export function nation(): object {    
    return {
        name: (): string => 'Indonesia',
        members: (): object[] => {
            return students().reduce((previous: object[], current: object): any => {
                return previous.concat({name: current['name'](), npm: current['npm']()})
            }, [])
        }
    };
};

export function printNation(): object {    
    return {
        name: nation()['name'](),
        members: nation()['members']()
    }
};

export const NationViews: Function = printNation;