import {
    students,
    printNation
} from "./views";
import chai, {
    expect
} from "chai";

chai.config.includeStack = true

//TEST FOR GETTING ALL MEMBERS
describe('Student Function', () => {
    it('should return group members', () => {
        expect(
            students().reduce((previous: object[], current: object) => {
                return previous.concat({
                    name: current['name'](),
                    npm: current['npm']()
                })
            }, [])).deep.equal(
            [{
                    name: 'Andika Hanavian Atmam',
                    npm: '1606918585'
                },
                {
                    name: 'Fari Qodri Andana',
                    npm: '1606875964'
                },
                {
                    name: 'Indra Septama',
                    npm: '1606892296'
                },
                {
                    name: 'Yumna Pratista Tastaftian',
                    npm: '1606836976'
                },
            ]
        )
    })
})


//TEST FOR GETTING NATION DATA
describe('JSON of nation data Function', () => {
    it('should return nation data', () => {
        expect(printNation()).deep.equal({
            name: 'Indonesia',
            members: [{
                    name: 'Andika Hanavian Atmam',
                    npm: '1606918585'
                },
                {
                    name: 'Fari Qodri Andana',
                    npm: '1606875964'
                },
                {
                    name: 'Indra Septama',
                    npm: '1606892296'
                },
                {
                    name: 'Yumna Pratista Tastaftian',
                    npm: '1606836976'
                }
            ]
        })
    })
})