[![pipeline status](https://gitlab.com/fariqodri/Pemfung/badges/Backend/pipeline.svg)](https://gitlab.com/fariqodri/Pemfung/commits/Backend)

# Repository Source Code Pemrograman Fungsional Kelompok "Pemfung"


**Anggota Kelompok:**

1606875964 - Fari Qodri Andana

1606892296 - Indra Septama

1606918585 - Andika Hanavian Atmam

1606836976 - Yumna Pratista Tastaftian

URL API: http://pemfung-api.herokuapp.com/api

URL: http://pemfung-indo.herokuapp.com
